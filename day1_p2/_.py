"""
https://adventofcode.com/2021/day/2

199  A
200  A B
208  A B C
210    B C D
200  E   C D
207  E F   D
240  E F G
269    F G H
260      G H
263        H

A (199, 200, 208); their sum is 199 + 200 + 208 = 607
B (200, 208, 210); its sum is 618

A: 607 (N/A - no previous sum)
B: 618 (increased)
C: 618 (no change)
D: 617 (decreased)
E: 647 (increased)
F: 716 (increased)
G: 769 (increased)
H: 792 (increased)
In this example, there are 5 sums that are larger than the previous sum.
"""

# from input0 import INPUT
from input import INPUT

lines = INPUT.strip().split('\n')

def next_sum(at_i):
    return int(lines[at_i]) + int(lines[at_i+1]) + int(lines[at_i+2])

try:    s_prev = next_sum(0)
except: s_prev = None

r = {
    True  : 0,
    False : 0,
}
for i in range(1, len(lines)-2 ):
    try:
        s = next_sum(i)
        r[s > s_prev] += 1
        s_prev = s
    except: pass

print(r[True])
