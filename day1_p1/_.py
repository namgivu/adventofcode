"""
https://adventofcode.com/2021/day/1

For example, suppose you had the following report:
199
200
208
210
200
207
240
269
260
263
In this example, there are 7 measurements that are larger than the previous measurement.
"""

# from input0 import INPUT
from input import INPUT

lines = INPUT.strip().split('\n')

try:    v_prev = int(lines[0])
except: v_prev = None

r = {
    True  : 0,
    False : 0,
}
for i in range(1, len(lines) ):
    v = int(lines[i])
    r[v>v_prev] += 1
    v_prev = v

print(r[True])
