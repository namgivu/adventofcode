"""
https://adventofcode.com/2021/day/3

00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010
Considering only the first bit of each number, there are five 0 bits and seven 1 bits. Since the most common bit is 1, the first bit of the gamma rate is 1.
The most common second bit of the numbers in the diagnostic report is 0, so the second bit of the gamma rate is 0.
The most common value of the third, fourth, and fifth bits are 1, 1, and 0, respectively, and so the final three bits of the gamma rate are 110.
So, the gamma rate is the binary number 10110, or 22 in decimal.

The epsilon rate is calculated in a similar way; rather than use the most common bit, the least common bit from each position is used. So, the epsilon rate is 01001, or 9 in decimal. Multiplying the gamma rate (22) by the epsilon rate (9) produces the power consumption, 198.
"""
import os

# with open(f'{os.path.dirname(__file__)}/input0.txt')    as f: lines = f.readlines()
with open(f'{os.path.dirname(__file__)}/input.txt')     as f: lines = f.readlines()


class Test:

    def test_part1(self):
        hor = 0
        dep = 0
        for l in lines:
            move, howmany = l.strip().split(' ') ; howmany = int(howmany)

            if False: pass
            elif move == 'forward'  : hor += howmany
            elif move == 'down'     : dep += howmany
            elif move == 'up'       : dep -= howmany

        print()
        print(hor * dep)

    def test_part2(self):
        hor = 0
        dep = 0
        aim = 0
        for l in lines:
            move, howmany = l.strip().split(' ') ; howmany = int(howmany)

            if False: pass
            elif move == 'down':    aim += howmany
            elif move == 'up':      aim -= howmany
            elif move == 'forward':
                hor +=       howmany
                dep += aim * howmany

            print(f'{hor=} {dep=} {aim=}')

        print()
        print(hor * dep)
