"""
https://adventofcode.com/2021/day/3

00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010
Considering only the first bit of each number, there are five 0 bits and seven 1 bits. Since the most common bit is 1, the first bit of the gamma rate is 1.
The most common second bit of the numbers in the diagnostic report is 0, so the second bit of the gamma rate is 0.
The most common value of the third, fourth, and fifth bits are 1, 1, and 0, respectively, and so the final three bits of the gamma rate are 110.
So, the gamma rate is the binary number 10110, or 22 in decimal.

The epsilon rate is calculated in a similar way; rather than use the most common bit, the least common bit from each position is used.
So, the epsilon rate is 01001, or 9 in decimal.

Multiplying the gamma rate (22) by the epsilon rate (9) produces the power consumption, 198.

--- part 2
The bit criteria depends on which type of rating value you want to find:
To find oxygen generator rating, determine the most common value (0 or 1) in the current bit position, and keep only numbers with that bit in that position. If 0 and 1 are equally common, keep values with a 1 in the position being considered.
To find CO2 scrubber rating, determine the least common value (0 or 1) in the current bit position, and keep only numbers with that bit in that position. If 0 and 1 are equally common, keep values with a 0 in the position being considered.

For example, to determine the oxygen generator rating value using the same example diagnostic report from above:
Start with all 12 numbers and consider only the first bit of each number. There are more 1 bits (7) than 0 bits (5), so keep only the 7 numbers with a 1 in the first position: 11110, 10110, 10111, 10101, 11100, 10000, and 11001.
Then, consider the second bit of the 7 remaining numbers: there are more 0 bits (4) than 1 bits (3), so keep only the 4 numbers with a 0 in the second position: 10110, 10111, 10101, and 10000.
In the third position, three of the four numbers have a 1, so keep those three: 10110, 10111, and 10101.
In the fourth position, two of the three numbers have a 1, so keep those two: 10110 and 10111.
In the fifth position, there are an equal number of 0 bits and 1 bits (one each). So, to find the oxygen generator rating, keep the number with a 1 in that position: 10111.
As there is only one number left, stop; the oxygen generator rating is 10111, or 23 in decimal.

Then, to determine the CO2 scrubber rating value from the same example above:
Start again with all 12 numbers and consider only the first bit of each number. There are fewer 0 bits (5) than 1 bits (7), so keep only the 5 numbers with a 0 in the first position: 00100, 01111, 00111, 00010, and 01010.
Then, consider the second bit of the 5 remaining numbers: there are fewer 1 bits (2) than 0 bits (3), so keep only the 2 numbers with a 1 in the second position: 01111 and 01010.
In the third position, there are an equal number of 0 bits and 1 bits (one each). So, to find the CO2 scrubber rating, keep the number with a 0 in that position: 01010.
As there is only one number left, stop; the CO2 scrubber rating is 01010, or 10 in decimal.

Finally, to find the life support rating, multiply the oxygen generator rating (23) by the CO2 scrubber rating (10) to get 230.

"""
import os


#region helper
def run_part1(lines):
    #region count bit
    r = []  # list of r[i]  # r[i] == count bit 0+1 at col i-th == dict{'0': count bit 0, '1': count bit 1, }

    l0 = lines[0].strip()
    maxcol = len(l0)
    for _ in range(maxcol): r.append({'0':0, '1':0})

    for l in lines:
        l = l.strip()

        for i,b in enumerate(l):  # b aka bit
            r[i][b]+=1
    #endregion count bit

    #region calc :gamma
    gamma_bit = []
    for i in range(maxcol): gamma_bit.append('')

    for i in range(maxcol):
        most_common_bit = '0' if r[i]['0']>r[i]['1'] else '1'
        gamma_bit[i]    = most_common_bit

    gamma_dec = int(''.join(gamma_bit), 2)

    print()
    print(f'{gamma_bit=}')
    print(f'{gamma_dec=}')
    #endregion calc :gamma

    #region calc :epsilon
    epsilon_bit = []
    for i in range(maxcol): epsilon_bit.append('')

    for i in range(maxcol):
        less_common_bit = '0' if r[i]['0']<r[i]['1'] else '1'
        epsilon_bit[i]  = less_common_bit

    epsilon_dec = int(''.join(epsilon_bit), 2)

    print()
    print(f'{epsilon_bit=}')
    print(f'{epsilon_dec=}')
    #endregion calc :epsilon

    result = gamma_dec * epsilon_dec
    print()
    print(result)
    return result


def run_part2(lines):
    maxcol = len(lines[0])

    #region calc oxy
    _lines = lines
    for col in range(maxcol):
        #region count bit at :col
        count_bit = {'0': 0, '1': 0}  # count bit 0+1 at col i-th
        for l in _lines:
            b = l[col]
            count_bit[b] += 1
        #endregion count bit at :col

        most_common_bit = '0' if count_bit['0']>count_bit['1'] else '1'

        _lines = [l for l in _lines if l[col]==most_common_bit]
        if len(_lines)==1:
            oxy_bit = _lines[0]
            break
    assert oxy_bit
    oxy_dec = int(oxy_bit, 2)
    print()
    print(f'{oxy_bit=}')
    print(f'{oxy_dec=}')
    #endregion calc oxy

    #region calc co2
    _lines = lines
    for col in range(maxcol):
        #region count bit at :col
        count_bit = {'0': 0, '1': 0}  # count bit 0+1 at col i-th
        for l in _lines:
            b = l[col]
            count_bit[b] += 1
        #endregion count bit at :col

        if   count_bit['0'] <  count_bit['1']: less_common_bit = '0'
        elif count_bit['0'] == count_bit['1']: less_common_bit = '0'
        else:                                  less_common_bit = '1'

        _lines = [l for l in _lines if l[col]==less_common_bit]
        if len(_lines)==1:
            co2_bit = _lines[0]
            break
    assert co2_bit
    co2_dec = int(co2_bit, 2)
    print()
    print(f'{co2_bit=}')
    print(f'{co2_dec=}')
    #endregion calc co2

    result = oxy_dec * co2_dec
    print()
    print(result)
    return result
#endregion helper


class Test:

    def test_part1__input0(self):
        with open(f'{os.path.dirname(__file__)}/input0.txt') as f: lines = f.readlines()
        r = run_part1(lines)
        assert r == 198

    def test_part1__input(self):
        with open(f'{os.path.dirname(__file__)}/input.txt') as f: lines = f.readlines()
        r = run_part1(lines)
        assert r == 2250414

    def test_part2__input0(self):
        with open(f'{os.path.dirname(__file__)}/input0.txt') as f: lines = f.readlines()
        lines = [l.strip() for l in lines]

        r = run_part2(lines)
        assert r == 230

    def test_part2__input(self):
        with open(f'{os.path.dirname(__file__)}/input.txt') as f: lines = f.readlines()
        lines = [l.strip() for l in lines]

        r = run_part2(lines)
        assert r == 6085575
